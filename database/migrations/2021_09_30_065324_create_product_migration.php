<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\{
    Schema,
    DB
};

class CreateProductMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->longText('description');
            $table->integer('messages');
            $table->float('price');
            $table->timestamps();
        });
        DB::table('product')->insert([
            [
                'name' => 'easy',
                'description' => 'All the basics for businesses that are just getting started.',
                'price' => 29.00,
                'messages' => 10
            ],
            [
                'name' => 'basic',
                'description' => 'Better for growing businesses that wnat more customers.',
                'price' => 59.00,
                'messages' => 100
            ],
            [
                'name' => 'custom',
                'description' => 'Advanced features for pros who need more customization.',
                'price' => 139.00,
                'messages' => 500
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}

