<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\{
    Schema,
    DB
};

class CreateAdvantages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advantages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('product_id')->constrained('product');
            $table->timestamps();
        });

        DB::table('advantages')->insert([
            ['name' => '10 messsages to send', 'product_id' => 1],
            ['name' => 'Email notification upon arrival at checkpoints', 'product_id' =>	1],
            ['name' => '100 messages to send', 'product_id' => 2],
            ['name' => 'Email notification upon arrival at checkpoints', 'product_id' => 2],
            ['name' => '500 messages to send', 'product_id' => 3],
            ['name' => 'Email notification upon arrival at checkpoints', 'product_id' => 3],
            ['name' => 'Online tracking', 'product_id' => 3]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advantages');
    }
}

