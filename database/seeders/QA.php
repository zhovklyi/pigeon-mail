<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\QA as QAmodel;

class QA extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QAmodel::factory(50)->create();
    }
}
