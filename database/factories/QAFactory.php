<?php

namespace Database\Factories;

//use App\Models\Model;
use App\Models\QA;
use Illuminate\Database\Eloquent\Factories\Factory;

class QAFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QA::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'question' => $this->faker->text(100),
            'answer' => $this->faker->text(),
            'like' => $this->faker->numberBetween(0, 125),
            'dislike' => $this->faker->numberBetween(0, 125)
        ];
    }
}
