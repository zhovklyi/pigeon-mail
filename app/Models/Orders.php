<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'stripe_id',
        'description',
        'payment_method',
        'receipt_email',
        'status',
        'product_id',
        'user_id'
    ];
}
