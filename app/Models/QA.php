<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QA extends Model
{
    use HasFactory;

    protected $table = 'QA';
    protected $hidden = array('created_at', 'updated_at');

    protected $fillable = [
        'question',
        'answer',
        'like',
        'dislike'
    ];
}
