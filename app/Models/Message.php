<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';

    protected $fillable = [
        'id',
        'text',
        'status',
        'user_id',
        'recipient_id'
    ];

    public function owner()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }

    public function recipient()
    {
        return $this->hasOne(Users::class, 'id', 'recipient_id');
    }
}
