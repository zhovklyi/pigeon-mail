<?php

namespace App\Http\Middleware;

use App\Models\Users;
use Closure;
use Illuminate\Http\Request;

class HasUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('User-Token');
        if (!$token) {
            return response()->json([
                'success' => false,
                'error_message' => 'No user token found'
            ]);
        }
        $user = Users::where('token', $token)->first();
        if (!$user){
            return response()->json([
                'success' => false,
                'error_message' => 'Wrong token value'
            ]);
        }
        return $next($request);
    }
}
