<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Product,
    Users,
    Orders
};

class SalesController extends Controller
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.key'));
    }

    // Buying product method
    public function buyProduct(Request $request, $id)
    {
        //dd($request->post());
        $token = $request->post('stripeToken');

        $product = Product::where('id', $id)->first();
        $user = Users::where('id', session('user')['id'])->first();

        $payment = \Stripe\Charge::create([
            'amount' => ($product->price)*100,
            'currency' => 'usd',
            'description' => "{$product->name} order for {$user->login}.",
            'receipt_email' => $user->email,
            'source' => $token,
        ]);

        if ($payment['status'] != "succeeded"){
            return redirect('/show/'.$id);
        }

        Orders::create([
            'stripe_id' => $payment['id'],
            'description' => $payment['description'],
            'payment_method' => $payment['payment_method'],
            'receipt_email' => $payment['receipt_email'],
            'status' => $payment['status'],
            'product_id' => $product->id,
            'user_id' => $user->id
        ]);

        $user->messages += $product->messages;
        $user->save();

        return redirect('/account');
        //dd($payment);
    }
}