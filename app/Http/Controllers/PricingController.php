<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Product,
    Users
};

class PricingController extends Controller
{
    // Displaying pricing page
    public function show()
    {
        return view('/blocks/main_pricing_body', [
            'products' => Product::with('advantages')->get()
        ]);
    }

    // Displays package order page
    public function showProduct($id)
    {
        return view('/blocks/product_pricing_body', [
            'product' => Product::with('advantages')->where('id', $id)->first()
        ]);
    }
}
