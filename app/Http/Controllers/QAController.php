<?php

namespace App\Http\Controllers;

use App\Models\QA;

class QAController extends Controller
{
    public function show()
    {
        return view('/blocks/QA_block');
    }

    public function getQA()
    {
        $qa = QA::all();
        return response()->json($qa);
    }
}
