<?php

namespace App\Http\Controllers;

use App\Http\Requests\SetUserDataRequest;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserDataRequest;

use App\Models\{
    Users,
    Product
};
use Illuminate\Support\Str;

class AccountController extends Controller
{
    // Account page
    public function account()
    {
        return view('/blocks/account_body', [
            'user' => Users::where('id', session('user')['id'])->first()
        ]);
    }

    // Loging out
    public function logOut(Request $request)
    {
        $request->session()->pull('user');
        return redirect('/pricing');
    }


    // Sign in GET method
    public function signInGet()
    {
        return view('/blocks/login_body');
    }

    // Sign in POST method
    public function signInPost(Request $request)
    {
        $login = $request->post('login');
        $password = hash('sha1', $request->post('password'));
        $user = Users::where(['login' => $login, 'password' => $password])->first();

        if (!$user){
            return redirect('/signin');
        }


        $request->session()->put('user', [
            'id' => $user->id,
            'login' => $user->login,
            'token' => $user->token
        ]);

        return redirect('/pricing');
    }

    // Sign up POST method
    public function signUpPost(SetUserDataRequest $request)
    {
        $validated = $request->validated();
        //dd($validated);
        $token = Str::random(16);
        $user = Users::create([
            'login' => $validated['login'],
            'name' => $validated['name'],
            'surname' => $validated['surname'],
            'email' => $validated['email'],
            'password' => hash('sha1', $validated['password']),
            'token' => hash('sha1', $token)
        ]);

        if (!$user){
            return redirect('/signup');
        }

        session()->put('user', [
            'id' => $user->id,
            'login' => $user->login,
            'token' => $user->token
        ]);

        return view('/blocks/save_token', [
            'token' => $token
        ]);
    }

    // Sign up GET method
    public function signUpGet(Request $request)
    {
        return view('/blocks/registration_body');
    }

    // Checking data from POST
    public function isValidData(UpdateUserDataRequest $request)
    {
        $validated = $request->validated();
        return response()->json([
            'result' => (bool) $validated
        ]);
    }

    // Check data before changing values
    public function setUserData(UpdateUserDataRequest $request)
    {
        $validated = $request->validated();

        $response['result'] = $this->setUserAttribute($validated);
        $response['message'] = $response['result']? 'Success': 'Error updating data';

        return response()->json($response);
    }

    // Setting user data
    public function setUserAttribute($data)
    {
        $user = Users::where('id', session('user')['id'])->first();
        foreach($data as $key => $value){
            $user->$key = $value;
            $user->save();
        }
        return true;
    }
}