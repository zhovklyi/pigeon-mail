<?php

namespace App\Http\Controllers;

use App\Http\Requests\{
    APIPostRequest,
    APIPutRequest
};
use App\Models\{
    Message,
    Users
};

use Illuminate\Http\Request;

class MessagesController extends Controller
{
    //Getting message list
    public function getMessages(Request $request)
    {
        $token = $request->header('User-Token');

        $owner_id = Users::where('token', $token)->first()->id;
        $messages = Message::where('user_id', $owner_id)
                            ->with('owner', 'recipient')
                            ->get()
                            ->map(function($message){
                                return [
                                    'id' => $message->id,
                                    'text' => $message->text,
                                    'status' => $message->status,
                                    'owner' => $message->owner->only('login', 'name', 'surname', 'email'),
                                    'recipient' => $message->recipient->only('login', 'name', 'surname', 'email'),
                                    'created_at' => $message->created_at,
                                    'updated_at' => $message->updated_at
                                ];
                            });

        return response()->json([
            'success' => true,
            'messages' => $messages
        ]);
    }

    // Getting message info
    public function getMessage($id)
    {
        $message = Message::where('id', $id)
                            ->with('owner', 'recipient')
                            ->get()
                            ->map(function($message){
                                return [
                                    'id' => $message->id,
                                    'text' => $message->text,
                                    'status' => $message->status,
                                    'owner' => $message->owner->only('login', 'name', 'surname', 'email'),
                                    'recipient' => $message->recipient->only('login', 'name', 'surname', 'email'),
                                    'created_at' => $message->created_at,
                                    'updated_at' => $message->updated_at
                                ];
                            });

        if (!$message){
            return response()->json([
                'success' => false,
                'error_message' => 'No such id found'
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $message->first()
        ]);
    }

    // Creating message info
    public function postMessage(APIPostRequest $request)
    {
        $token = $request->header('User-Token');
        $request = $request->validated();

        $message = Message::create([
            'text' => $request['text'],
            'status' => 'Send',
            'user_id' => Users::where('token', $token)->first()->id,
            'recipient_id' => Users::where('email', $request['recipient_email'])->first()->id
        ]);

        if (!$message) {
            return response()->json([
                'status' => false,
                'error_message' => 'Error creating message'
            ]);
        }
        $message = Message::where('id', $message->id)
                            ->with('owner', 'recipient')
                            ->get()
                            ->map(function($message){
                                return [
                                    'id' => $message->id,
                                    'text' => $message->text,
                                    'status' => $message->status,
                                    'owner' => $message->owner->only('login', 'name', 'surname', 'email'),
                                    'recipient' => $message->recipient->only('login', 'name', 'surname', 'email'),
                                    'created_at' => $message->created_at,
                                    'updated_at' => $message->updated_at
                                ];
                            });

        return response()->json([
            'status' => true,
            'message' => $message->first()
        ]);
    }

    // Updating meessage info
    public function putMessage(APIPutRequest $request, $id)
    {
        $token = $request->header('User-Token');
        $request = $request->validated();

        $message = Message::find($id);

        if ($message->user_id != Users::where('token', $token)->first()->id){
            return response()->json([
                'status' => false,
                'error_message' => 'Permission denied'
            ]);
        }

        foreach($request as $key => $value){
            $message->$key = $value;
        }
        $message->save();

        $message = Message::where('id', $message->id)
                            ->with('owner', 'recipient')
                            ->get()
                            ->map(function($message){
                                return [
                                    'id' => $message->id,
                                    'text' => $message->text,
                                    'status' => $message->status,
                                    'owner' => $message->owner->only('login', 'name', 'surname', 'email'),
                                    'recipient' => $message->recipient->only('login', 'name', 'surname', 'email'),
                                    'created_at' => $message->created_at,
                                    'updated_at' => $message->updated_at
                                ];
                            });

        return response()->json([
            'status' => true,
            'message' => $message
        ]);
    }

    public function deleteMessage(Request $request, $id)
    {
        $token = $request->header('User-Token');
        
        $message = Message::find($id);

        if ($message->user_id != Users::where('token', $token)->first()->id){
            return response()->json([
                'status' => false,
                'error_message' => 'Permission denied'
            ]);
        }

        $message_deleted = Message::where('id', $message->id)
                                    ->with('owner', 'recipient')
                                    ->get()
                                    ->map(function($message){
                                        return [
                                            'id' => $message->id,
                                            'text' => $message->text,
                                            'status' => $message->status,
                                            'owner' => $message->owner->only('login', 'name', 'surname', 'email'),
                                            'recipient' => $message->recipient->only('login', 'name', 'surname', 'email'),
                                            'created_at' => $message->created_at,
                                            'updated_at' => $message->updated_at
                                        ];
                                    });

        $message->delete();

        return response()->json([
            'status' => true,
            'message' => $message_deleted->first()
        ]);
    }
}