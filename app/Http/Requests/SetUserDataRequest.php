<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetUserDataRequest extends FormRequest
{
    public function rules()
    {
        return [
            'login' => 'required|unique:App\Models\Users,login',
            'email' => 'required|unique:App\Models\Users,email',
            'password' => 'required',
            'name' => 'required',
            'surname' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Login field is empty',
            'login.unique' => 'This login is already in use',
            'email.required' => 'Email field is empty',
            'email.unique' => 'This email is already in use',
            'password.required' => 'Password field is empty',
            'name.required' => 'Name field is empty',
            'surname.required' => 'Surname field is empty'
        ];
    }
}
