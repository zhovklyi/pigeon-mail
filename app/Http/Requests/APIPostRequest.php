<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class APIPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient_email' => 'required|exists:users,email',
            'text' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'recipient_email.required' => 'Empty email',
            'recipient_email.exists' => 'User not found',
            'text.required' => 'Empty text'
        ];
    }
}
