<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserDataRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'sometimes|required|unique:App\Models\Users,login',
            'email' => 'sometimes|required|unique:App\Models\Users,email',
            'password' => 'sometimes|required',
            'name' => 'sometimes|required',
            'surname' => 'sometimes|required'
        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Login field is empty',
            'login.unique' => 'This login is already in use',
            'email.required' => 'Email field is empty',
            'email.unique' => 'This email is already in use',
            'password.required' => 'Password field is empty',
            'name.required' => 'Name field is empty',
            'surname' => 'Surname field is empty'
        ];
    }
}
