<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class APIPutRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient_email' => 'sometimes|required|exists:users,email',
            'text' => 'sometimes|required',
            'status' => 'sometimes|required'
        ];
    }

    public function messages()
    {
        return [
            'recipient_email.required' => 'Empty email',
            'recipient_email.exists' => 'No user found',
            'text.required' => 'Empty text',
            'status.required' => 'Empty status value'
        ];
    }
}
