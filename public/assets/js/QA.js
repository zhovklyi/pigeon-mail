'use strict';

const app = {
    data(){
        return {
            items: []
        }
    },
    methods:{
        async requestItems(){
            const response = await fetch('/qa/get', {
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                    Accept: 'applictaion/json',
                    'Content-Type': 'applictaion/json'
                }
            });

            const json = await response.json();

            return json;
        },
        changeAnswerState(item){
            item.isAnswerShown = !item.isAnswerShown;
        },
        async like(item){
            const response = await fetch('/qa/like/' + item.id, {
                method: 'GET',
                headers:{
                    Accept: 'applictaion/json',
                    'Content-Type': 'applictaion/json'
                }
            });
            const json = await response.json();

            if (!json['result']){
                return;
            }
            item.like = json['value'];
        },
        async dislike(item){
            const response = await fetch('/qa/dislike/' + item.id, {
                method: 'GET',
                headers:{
                    Accept: 'applictaion/json',
                    'Content-Type': 'applictaion/json'
                }
            });
            const json = await response.json();

            if (!json['result']){
                return;
            }
            item.dislike = json['value'];
        }
    },
    async created(){
        this.items = await this.requestItems();
        this.items.forEach(element => {
            element.isAnswerShown = false;
        });
    }
}

Vue.createApp(app).mount('#QA-block');