'use strict';

function validateEmail(email) {
    let regExp = /\S+@\S+\.\S+/;
    return regExp.test(email);
}

let inputs = Array.from(document.querySelectorAll('input'));
inputs = inputs.filter(input => (input.type != 'checkbox' && input.type != 'hidden'));

console.log(inputs);

function changeClass(element, newClass, oldClass){
    if (element.classList.contains(oldClass)){
        element.classList.replace(oldClass, newClass);
        return;
    }
    element.classList.add(newClass);
}

for (let element of inputs){
    element.onchange =  async function() {
        if (this.id == 'email' && (!validateEmail(this.value))) {
            changeClass(this, 'notavailable', 'available');
            return;
        }

        const response = await fetch('/isValidData', {
            method: 'POST',
            body: JSON.stringify({
                [this.id]: this.value
            }),
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                Accept: 'applictaion/json',
                'Content-Type': 'applictaion/json'
            }
        });

        const json = await response.json();

        if (json['result']){
            changeClass(this, 'available', 'notavailable');
            return;
        }
        changeClass(this, 'notavailable', 'available');
    }
}