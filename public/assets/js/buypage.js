'use strict';

let button = document.querySelector('#payment');
button.onclick = () => {
  document.querySelector('.popup').style.display = 'block';
}

let stripe = Stripe('pk_test_51JeHwYCSMk31MGMJSciIsSQ1C0gWt5rPXOlpj2WJf0ukbbSBHTxY3mwUtbRKhkJ3wXgc4O5rjznl3qarK07SWNIT005zzCHaRk');

let elements = stripe.elements();
var style = {
    base: {
        fontSize: '16px',
        color: '#32325d',
    },
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// next
// Create a token or display an error when the form is submitted.
var form = document.querySelector('#payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.querySelector('.card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

//

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.querySelector('#payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
}