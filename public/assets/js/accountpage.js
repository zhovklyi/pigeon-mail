'use strict';

const app = {
    data(){
        return {
            login: {
                isEditing: false,
                name: 'login',
                primeValue: initialData.login,
                hasErrors: false,
                errorMessage: ''
            },
            name: {
                isEditing: false,
                name: 'name',
                primeValue: initialData.name,
                hasErrors: false,
                errorMessage: ''
            },
            surname: {
                isEditing: false,
                name: 'surname',
                primeValue: initialData.surname,
                hasErrors: false,
                errorMessage: ''
            },
            email: {
                isEditing: false,
                name: 'email',
                primeValue: initialData.email,
                hasErrors: false,
                errorMessage: ''
            }
        }
    },
    methods:{
        // Enabling\Disabling editing
        changeState(attribute){
            attribute.isEditing = !attribute.isEditing;
            attribute.value = attribute.primeValue;
        },
        // Is changes are done sets new value
        changeSuccess(attribute){
            attribute.primeValue = attribute.value;
            attribute.hasErrors = false;
            this.changeState(attribute);
        },
        // For discard editing and setting default value
        changeDiscard(attribute){
            attribute.value = attribute.primeValue;
            attribute.hasErrors = false;
            this.changeState(attribute);
        },
        async saveValues(attribute){
            const response = await fetch('/account/set', {
                method: 'POST',
                body: JSON.stringify({
                    [attribute.name]: attribute.value
                }),
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                    Accept: 'applictaion/json',
                    'Content-Type': 'applictaion/json'
                }
            });
            const json = await response.json();

            if (!json['result']){
                attribute.hasErrors = true;
                attribute.errorMessage = json['errors'][attribute.name][0];
                return;
            }

            this.changeSuccess(attribute);
        }
    },
    mounted(){
        ['login', 'name', 'surname', 'email'].every(
            key => this[key].value = this.$refs[key].dataset.value
        );
    }
}
Vue.createApp(app).mount('#account-info');