<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pigeon Mail</title>
        <link rel="stylesheet" href="/assets/css/page.css">
        <!-- Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <!-- Stripe -->
        <script src="https://js.stripe.com/v3/"></script>
    </head>
    <body>
    <div class="page_container">
        @if (!isset($header_disabled))
        <div class="header">
            <div class="logo_block">
                <div class="logo">
                    <img src="/assets/img/pigeon_logo.png">
                </div>
                <div class="name">
                    Pigeon Mail
                </div>
            </div>
            <div class="navigation_block">
                <a href="/">Home</a>
                <a href="/pricing" class="active">Pricing</a>
                <a href="/messages">API</a>
                <a href="/qa">QA</a>
            </div>
            <div class="login_box">
                @if (session('user', false))
                    <a href="/account">{{session('user')['login']}}</a>
                    <a href="/logout">Log out</a>
                @else
                    <a href="/signin">Login</a>
                    <a href="/signup">Get started</a>
                @endif
            </div>
        </div>
        @endif
        @yield('body')
    </div>
    </body>
</html>
