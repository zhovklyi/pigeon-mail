<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Pigeon Mail</title>
        <!-- Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    </head>
    <body>
    <link rel="stylesheet" href="/assets/css/mainpage.css">
    <div class="page1">
        <div class="header">
            <a href="#about">About</a>
            <a href="#team">Our team</a>
            <a href="/pricing">Pricing</a>
            <a href="/messages">API</a>
        </div>
        <div class="logo_block">
            <div class="text_block">
                <div class="slogan_block">
                    <p class="p1">Eazy.</p>
                    <p class="p2">Quick.</p>
                    <p class="p3">Modern.</p>
                </div>
                <div class="achivment_block">
                    <p>World`s First Pigeon Mail API</p>
                </div>
            </div>
            <div class="vector_block">
                <div class="vector_img"></div>
            </div>
        </div>
    </div>
    <a name="about"></a>
    <div class="page2">
        <div class="qacontainer">
            <div class="qa_block">
                <p class="question">You see the birds?</p>
                <p class="answer">We see data units!</p>
            </div>
        </div>
        <div class="description_block">
            <div class="p_container">
                <p class="p1">Powerful technology was reinvented</p>
                <p class="p2">to provide you totally new experience</p>
            </div>
        </div>
    </div>

    <a name="team"></a>
    <div class="page3">
        <div class="team_header">
            <p>OUR TEAM</p>
        </div>
        <div class="team_block">
            <div class="team_member">
                <div class="team_member_photo">
                    <img src="/assets/img/bird1.png">
                </div>
                <div class="team_member_name_postition">
                    <div class="team_member_name">
                        Charlie
                    </div>
                    <div class="team_member_position">
                        Head of dovecote
                    </div>
                </div>
                <div class="team_member_description">
                    As a head of dovecote, Charlie shows excellent skills of team management.His life motto: "Coo, coo-coo coo,cooo-ccrrooo coo!", and we think itperfectly describes him
                </div>
            </div>
            <div class="team_member">
                <div class="team_member_photo">
                    <img src="/assets/img/bird2.png">
                </div>
                <div class="team_member_name_postition">
                    <div class="team_member_name">
                        Daisy
                    </div>
                    <div class="team_member_position">
                        Navigation Manager
                    </div>
                </div>
                <div class="team_member_description">
                    Daisy perfectly knows the roadmap for all flights, and for our company at all. She can guarantee, that our service is safe to “hawk-in-the-middle” attack
                </div>
            </div>
            <div class="team_member">
                <div class="team_member_photo">
                    <img src="/assets/img/bird3.png">
                </div>
                <div class="team_member_name_postition">
                    <div class="team_member_name">
                        Jack
                    </div>
                    <div class="team_member_position">
                        Senior Engineer
                    </div>
                </div>
                <div class="team_member_description">
                    Pigeons maintenance, fixes, training - all of that is his job. He is said to be 50% man and 50% pigeon, but we don't care if that's truth
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <p>Small footer should be here</p>
    </div>
    </body>
</html>
