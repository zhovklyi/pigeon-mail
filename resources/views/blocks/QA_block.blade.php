@extends('pages.page')
@section('body')
<script src="https://unpkg.com/vue@next"></script>
<link rel="stylesheet" href="/assets/css/QA.css">
<div class="body">
    <div id="QA-block">
        <div class="QA-container">
            <ul>
                <li v-for="item in items">
                    <ul class="QA-row">
                        <li class="top">
                            <div class="question">@{{item.question}}</div>
                            <div class="likes-dislikes-block">
                                <img src="/assets/img/logo/like.png" class="like" @click="like(item)">
                                <div class="like-dislike">@{{item.like}}/@{{item.dislike}}</div>
                                <img src="/assets/img/logo/dislike.png" class="dislike" @click="dislike(item)">
                            </div>
                            <div class="expand" @click="changeAnswerState(item)">V</div>
                        </li>
                        <li class="bottom" v-show="item.isAnswerShown">
                            <ul>
                                <li>@{{item.answer}}</li>
                                <li>
                                    <div class="controlls">
                                        <img src="/assets/img/logo/like.png" class="like" @click="like(item)">
                                        <img src="/assets/img/logo/dislike.png" class="dislike" @click="dislike(item)">
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<script src="/assets/js/QA.js"></script>
@endsection