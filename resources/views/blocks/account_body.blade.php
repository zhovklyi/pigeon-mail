@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/account_body.css">
<!-- Vue -->
<script src="https://unpkg.com/vue@next"></script>
<style>
    [v-cloak]{
        display: none;
    }
</style>

<div class="body">
    <div class="account-settings">
        <div class="account-header">Account info</div>
        <div class="account-infos" id="account-info">
            <div class="info-row">
                <div class="data-container">
                    <label>Login</label>
                    <div class="data-row" v-cloak>
                        <label class="data-label" v-show="!login.isEditing" @dblclick="changeState(login)">@{{login.primeValue}}</label>
                        <input v-show="login.isEditing" data-value="{{$user->login}}" v-model="login.value" ref="login">
                        <label class="error" v-show="login.hasErrors">@{{login.errorMessage}}</label>
                        <div class="controlls" v-show="login.isEditing">
                            <button class="edit" @click="saveValues(login)">Edit</button>
                            <button class="cancel" @click="changeDiscard(login)">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-row">
                <div class="data-container">
                    <label>Name</label>
                    <div class="data-row" v-cloak>
                        <label class="data-label" v-show="!name.isEditing" @dblclick="changeState(name)">@{{name.primeValue}}</label>
                        <input v-show="name.isEditing" data-value="{{$user->name}}" v-model="name.value" ref="name" >
                        <label class="error" v-show="name.hasErrors" >@{{name.errorMessage}}</label>
                        <div class="controlls" v-show="name.isEditing" >
                            <button class="edit" @click="saveValues(name)">Edit</button>
                            <button class="cancel" @click="changeDiscard(name)">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-row">
                <div class="data-container">
                    <label>Surname</label>
                    <div class="data-row" v-cloak>
                        <label class="data-label" v-show="!surname.isEditing" @dblclick="changeState(surname)">@{{surname.primeValue}}</label>
                        <input v-show="surname.isEditing" data-value="{{$user->surname}}" v-model="surname.value" ref="surname" >
                        <label class="error" v-show="surname.hasErrors" >@{{surname.errorMessage}}</label>
                        <div class="controlls" v-show="surname.isEditing" >
                            <button class="edit" @click="saveValues(surname)">Edit</button>
                            <button class="cancel" @click="changeDiscard(surname)">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-row">
                <div class="data-container">
                    <label>Email</label>
                    <div class="data-row" v-cloak>
                        <label class="data-label" v-show="!email.isEditing" @dblclick="changeState(email)">@{{email.primeValue}}</label>
                        <input v-show="email.isEditing" data-value="{{$user->email}}" v-model="email.value" ref="email" >
                        <label class="error" v-show="email.hasErrors" >@{{email.errorMessage}}</label>
                        <div class="controlls" v-show="email.isEditing" >
                            <button class="edit" @click="saveValues(email)">Edit</button>
                            <button class="cancel" @click="changeDiscard(email)">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="account-products">
        <div class="account-header">Account products</div>
        <div class="account-products">

        </div>
    </div>
</div>
<script>
    let initialData = {
        login: "{{$user->login}}",
        name: "{{$user->name}}",
        surname: "{{$user->surname}}",
        email: "{{$user->email}}"
    };
</script>
<script src="/assets/js/accountpage.js"></script>
@endsection