@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/login_register.css">
<div class="body">
    <div class="image_block">
        <div class="redirect_block">
            <div class="message">Already have accout?</div>
            <a href="/signin">Sign In</a>
        </div>
    </div>
    <div class="form_block">
        <div class="form_container">
            <div class="header">Sign Up</div>
            <div class="form">
                <div class="token">
                    <div class="accept logo"></div>
                    <div class="message">Save this token in some place. It`s required for future operations.</div>
                    <div class="token">{{$token}}</div>
                    <a href="/account">Continue</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/registrationpage.js"></script>
@endsection