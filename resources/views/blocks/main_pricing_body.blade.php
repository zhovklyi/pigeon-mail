@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/pricing_main.css">
<div class="body">
    <div class="body_header">
        <div class="heading">
            <p>Our <b>plans</b> for your <b>strategies</b>.</p>
        </div>
        <div class="description">
            <p>See below our three main plans for your business, for your startup and agency. It starts from here! you can teach yourself what you really like.</p>
        </div>
    </div>
    <div class="products_row">
        @foreach($products as $product)
            <div class="product">
                <div class="product_container">
                    <div class="price">
                        <div class="price_value">${{$product->price}}</div>
                    </div>
                    <div class="product_type">
                        <div class="type_name">{{$product->name}}</div>
                        <div class="type_description">{{$product->description}}</div>
                    </div>
                    <div class="advantages">
                        @foreach($product->advantages as $advantage)
                            <div class="advantage_row">
                                <img src="/assets/img/check.png">
                                <div class="advantage">{{$advantage->name}}</div>
                            </div>
                        @endforeach
                    </div>
                    <div class="controll">
                        <a href="/pricing/show/{{$product->id}}">Get {{$product->name}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection