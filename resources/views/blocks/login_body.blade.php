@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/login_register.css">
<div class="body">
    <div class="image_block">
        <div class="redirect_block">
            <div class="message">Don`t have accout?</div>
            <a href="/signup">Sign Up</a>
        </div>
    </div>
    <div class="form_block">
        <div class="form_container">
            <div class="header">Sign In</div>
            <div class="form">
                <form action="/signin" method="POST">
                    <div class="form_row">
                        <label for="login" class="block">Login</label>
                        <input id="login" type="text" name="login" placeholder="Enter your login" required>
                    </div>
                    <div class="form_row">
                        <label for="password" class="block">Password</label>
                        <input id="password" type="password" name="password" placeholder="Enter your password" required>
                    </div>
                    <div class="form_row">
                        <input id="checkbox" type="checkbox">
                        <label for="checkbox">Show password</label>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form_row">
                        <button type="submit">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection