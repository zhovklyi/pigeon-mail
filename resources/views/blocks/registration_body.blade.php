@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/login_register.css">
<div class="body">
    <div class="image_block">
        <div class="redirect_block">
            <div class="message">Already have accout?</div>
            <a href="/signin">Sign In</a>
        </div>
    </div>
    <div class="form_block">
        <div class="form_container">
            <div class="header">Sign Up</div>
            <div class="form">
                <form action="/signup" method="POST">
                    <div class="form_row">
                        <label for="login" class="block">Login</label>
                        <input id="login" type="text" name="login" placeholder="Enter your login" required>
                    </div>
                    <div class="form_row">
                        <label for="name" class="block">Name</label>
                        <input id="name" type="text" name="name" placeholder="Enter your name" required>
                    </div>
                    <div class="form_row">
                        <label for="surname" class="block">Surname</label>
                        <input id="surname" type="text" name="surname" placeholder="Enter your surname" required>
                    </div>
                    <div class="form_row">
                        <label for="email" class="block">Email</label>
                        <input id="email" type="email" name="email" placeholder="Enter your email" required>
                    </div>
                    <div class="form_row">
                        <label for="password" class="block">Password</label>
                        <input id="password" type="password" name="password" placeholder="Enter your password" required>
                    </div>
                    <div class="form_row">
                        <label for="password_confirm" class="block">Password confirmation</label>
                        <input id="password_confirm" type="password" name="password_confirm" placeholder="Confirm your password" required>
                    </div>
                    <div class="form_row">
                        <input id="checkbox" type="checkbox">
                        <label for="checkbox">Show password</label>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form_row">
                        <button type="submit">Sign Un</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/registrationpage.js"></script>
@endsection