@extends('pages.page')
@section('body')
<link rel="stylesheet" href="/assets/css/pricing_product.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Michroma&display=swap" rel="stylesheet">
<div class="body">
    <div class="product">
        <div class="product_container">
            <div class="price">
                <div class="price_value">${{$product->price}}</div>
            </div>
            <div class="product_type">
                <div class="type_name">{{$product->name}}</div>
                <div class="type_description">{{$product->description}}</div>
            </div>
            <div class="advantages">
                @foreach($product->advantages as $advantage)
                    <div class="advantage_row">
                        <img src="/assets/img/check.png">
                        <div class="advantage">{{$advantage->name}}</div>
                    </div>
                @endforeach
            </div>
            <div class="controll">
                <a id="payment">Buy {{$product->name}}</a>
            </div>
        </div>
    </div>
</div>

<div class="popup">
    <div class="popup__body">
        <div class="popup_content">
            <form action="/buy/{{$product->id}}" method="post" id="payment-form">
                <div class="form-row">
                    <label for="card-element">Credit or debit card</label>
                    <div id="card-element"></div>
                    <div id="card-errors" role="alert"></div>
                </div>
                <button>Submit Payment</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
        </div>
    </div>
</div>
<script src="/assets/js/buypage.js"></script>
@endsection