<?php

use App\Http\Middleware\{
    HeaderDisabled,
    UserAuthorised
};
use App\Models\QA;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('/pages/main_page');
});

// Routs for pricing page manipulations
Route::prefix('/pricing')->group(function(){
    Route::get('/', 'PricingController@show');
    Route::get('/show/{id}', 'PricingController@showProduct')->whereNumber('id')->middleware(UserAuthorised::class);});

// Routs for sales controller
Route::post('/buy/{id}', 'SalesController@buyProduct')->whereNumber('id')->middleware(UserAuthorised::class);

// Routes for account manipulations
Route::middleware(HeaderDisabled::class)->group(function(){
    Route::get('/signin', 'AccountController@signInGet');
    Route::post('/signin', 'AccountController@signInPost');
    Route::get('/signup', 'AccountController@signUpGet');
    Route::post('/signup', 'AccountController@signUpPost');
});

Route::prefix('/account')->group(function(){
    Route::get('/', 'AccountController@account')->middleware(UserAuthorised::class);
    Route::post('/set', 'AccountController@setUserData');
});

Route::get('/logout', 'AccountController@logOut');
Route::post('/isValidData', 'AccountController@isValidData');

// Q\A routes
Route::prefix('/qa')->group(function(){
    Route::get('/', 'QAController@show');
    Route::get('/get', 'QAController@getQA');
    Route::get('/like/{qa}', function(QA $qa){
        $qa->like += 1;
        $qa->save();
        return response()->json([
            'result' => true,
            'value' => $qa->like
        ]);
    });
    Route::get('/dislike/{qa}', function(QA $qa){
        $qa->dislike += 1;
        $qa->save();
        return response()->json([
            'result' => true,
            'value' => $qa->dislike
        ]);
    });
});