<?php

use App\Http\Middleware\HasUserToken;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::middleware(HasUserToken::class)->group(function(){
    Route::get('/messages', 'MessagesController@getMessages');
    Route::get('/message/{id}', 'MessagesController@getMessage')->whereNumber('id');
    Route::post('/message', 'MessagesController@postMessage');
    Route::put('/message/{id}', 'MessagesController@putMessage')->whereNumber('id');
    Route::delete('/message/{id}', 'MessagesController@deleteMessage')->whereNumber('id');
});